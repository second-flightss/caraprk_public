# 停车场管理_充电桩_停车收费_物业管理_物联网_自助缴费

 **【小程序成品展示】：** 

我们团队目前已经实战数据：300家真实停车场(小区，别墅，商业街，商场，园区)，数据量达15000000，收缴停车费 人民币100000000元+，若您是投资者，请联系我电话：18086495676，若您名下有多个停车场需要管理的，也可以拨打我的电话，我们帮您运营！

![输入图片说明](gh_421c73099590_258%20(1).jpg)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/231938_b51c9eea_753802.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/232015_859edbf1_753802.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/232038_e4aa4c53_753802.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/232059_28c8ff52_753802.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/232209_6a6eade7_753802.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0811/232225_6c28fc6a_753802.png "屏幕截图.png")

【声明】：本项目里面的代码没有任何私jar包，本来就是开源项目，代码没有丢包或者故意丢代码导致项目报错的情况，但是不保证能商用，个人拿来作为入门研究和快速二次开发是个不错的选择，想要商用的建议自己进行二次开发。对于那些想让我免费二次的开发或者免费提供商业版本而且说话还很难听的白眼狼，请绕道！这项目代码肯定能跑起来，技术不到家的，自己先去学习学习java基础和springboot基础！

【 **互动交流** 】：想商业合作的加我微信: **Dove981011512**  非诚勿扰，小白和学生建议请自行克隆源码研究，谢谢配合！

有能力的就购买商业版，如果资金困难，可以自己在此项目代码上进行二次开发！

高级商业版官方网址：https://www.cfeng.wang

【 **商业测试** 】：我们不是卖设备的，但是这类项目是物联网系统，没有设备的测试都将毫无意义，想实际查看商业级别效果的，自己购买设备接入系统测试整个流程，纯账号一顿点的无聊之人我们很忙，如果你自己本身已有设备可以直接免费接入测试，无需再购买，自行购买设备某宝地址：https://m.tb.cn/h.fnPsWrs?tk=ZZc22U3r8uJ

详细在线介绍和部署文档【推荐仔细查阅】:https://www.showdoc.cc/902821218399318?page_id=4812017724186215

前端小程序开源地址：https://gitee.com/wangdefu/parking_system_applet

【我们的优势】：专业IT团队打造，多年停车场研究领域专家，资深架构师部署架构。真实百万级别项目在线运营实践！大集团VIP供应商！我们的目标：一套停车场系统，管理全球停车场，管理与分账两不误！最近我们新增了充电桩功能模块，欢迎大家体验

#### 【功能介绍】：
①本停车场系统兼容市面上主流的多家相机，理论上兼容所有硬件，可灵活扩展，②相机识别后数据自动上传到云端并记录，校验相机唯一id和硬件序列号，防止非法数据录入，③用户手机查询停车记录详情可自主缴费(支持微信，支付宝，银行接口支付，支持每个停车场指定不同的商户进行收款)，支付后出场在免费时间内会自动抬杆。④支持app上查询附近停车场(导航，可用车位数，停车场费用，优惠券，评分，评论等)，可预约车位。⑤断电断网支持岗亭人员使用app可接管硬件进行停车记录的录入。
#### 【技术架构】：
后端开发语言java，框架oauth2+springboot2+doubble2.7.3，数据库mysql/mongodb/redis，即时通讯底层框架netty4，安卓和ios均为原生开发，后台管理模板vue-typescript-admin-template,文件服务fastDFS，短信目前仅集成阿里云短信服务。为千万级数据而生，千万级用户无忧，目前真实用户40w无压力，大数据时代物联网必备
#### 【开源情况】：
代码完全开源，不存在授权问题，完全自主原创，不存在任何后门，不使用任何第三方私有jar包，性能和安全完全自主可控，想怎么耍就这么耍，就是这么任性，后续更新的话本人会持续更新部署教程。代码专业规范，新手看得懂，高手喜欢用。本系统完全免费
#### 【部署环境】：
目前仅测试linux环境一切正常，win环境没部署过，演示地址在本文章末尾
#### 【关于作者】：
屌丝码农一枚，4年前曾就职于开发停车场系统的公司，发现目前国内该领域垄断，技术过于陈旧，没有一个规范，故个人用来接近1年的时间在业余时间开发出这种系统，现代化标准的互联网应用，定位大型物联网大数据云平台系统，我个人微信Dove981011512，如果你在部署我这套系统中遇到问题或者发现存在漏洞的请联系我，存在不足之处还望多多提宝贵意见，让我们打破市场垄断，让物联网应用更好的服务生活社会

#### 软件架构
一、技术构成简述
（一）编程语言与架构简述
1．开发语言
（1）服务端
服务端语言目前均采用java语言开发，jdk版本要求1.8+。开发框架为springboot2+dubbo，鉴权采用oauth2，DB操作框架Mybaits，即时通讯底层框架与协议netty4

（2）客户端
目前我们主要客户端分为三个场景，分别为安卓，ios，微信公众号。安卓与ios均为原生开发，H5页面web端框架为vue

（3）后台管理
后台管理前端框架采用的是主流的vue element admin(TypeScript版本)，分层清晰，官方文档完整，社区活跃

2．数据存储
（1）重要数据存储
重要数据均采用mysql进行存储，支持部署主从，大部分数据尽可能进行事务处理，确保数据容灾性

（2）一般数据存储
非重要性数据例如聊天内容，系统消息通知，广告等数据均存储于mongodb数据库中

（3）缓存数据存储
微小量缓存会存在mysql中，例如评论的前N条评论快照会超小量进行字段适当冗余，在提高存储性价比情况下大大提高数据的查询能力。其它大部分数据缓存均存储于redis数据中

3．性能与安全
（1）性能解决方案
架构与技术解决方案均为本团队一线5年开发经验总结，目前我们正在接触的项目真实用户40w+，毫无压力，我们系统采用的架构与技术均在仔细多方面综合考虑后多次调整，采用更加合理，性能更佳的模式与解决方案

（2）安全解决方案
所有请求均需携带jwt串token进行访问，每个接口服务和管理服务均需配置公钥文件且具有jwt串token合法性校验能力，用户权限服务携带私钥文件负责密钥生成

4．架构与生命力
（1）采用架构
本系统采用阿里巴巴微服务框架dubbo来进行实现微服务提供能力，追求高性能，高可用，超细粒度独立微服务，相同服务可以动态灵活增加与减少，支持不停机发布新版本服务。每个服务之间均为独立存在，互不影响。例如短信发送，支付，订单，停车场系统接口，停车场后台管理，停车场提供者服务等均为独立的服务。

（2）架构潜力
整个系统众多服务分工明确，细粒度微服务，实现真正的插拔服务，功能的删减或停用，新增等均可在不破坏和入侵原来系统的前提下满足新的开发需求

5．二次开发说明
（1）适用客户对象
①本身有互联网it编程技术和经验或者拥有技术团队的。
②不具备第一个条件但是费用预算比较充足，二次开发需求少或者愿意支付高额定制费的
（2）团队要求
服务器运维，安卓与ios开发者，web前端开发者，java实际开发经验2年+开发者

（3）技术要求
过硬的java编程能力，网络编程能力，数据库设计与优化能力，架构设计能力，微服务思维能力，成熟的前端技术开发能力，中大型系统部署与运营能力

（4）硬件要求
Linux操作系统，8核16G(最低)5M带宽，可多台服务器中的微服务指向统一微服务调度中心(本系统微服务调度中心管理平台zookeeper)

（二）软件与硬件数据交互简述
1．硬件端
（1）目前解决方案
封装工具类，兼容市场主流硬件设备，只负责各类硬件数据封装为统一数据结构。硬件发包目前多为http主动推送数据，被动接受服务端返回指令

（2）未来解决方案
改造主流厂商硬件底层服务系统，新增硬件规范的合法身份数据，采用长连接进行数据交互，保证数据与指令的实时性与可靠性得到更好的保障

2．服务端
（1）被动处理硬件数据
中间件处理各类前端数据，接收硬件推送数据，解析，计算，做出相应反馈

（2）主动通知硬件发生事件行为
长连接推送指令，例如开闸，实时动态配置硬件数据等，

二、常规功能简述
（一）基础功能
1．硬件管理
支持单个硬件管理与记录，硬件在线状态，维修与进度记录等。与指定停车场出入口进行绑定，均有记录GPS位置

2．停车场管理
不同时段费用配置，每日封顶因素综合参与动态计费，也支持静态+每日上限计费。支持查询附近停车场功能

3．停车记录管理
详细记录产生时间，地点，进出口位置，进出时间，异常数据实时推送与快速处理

4．支付机构管理
每个停车场的支付账号均可以独立配置，支持同一个停车场使用多家支付机构进行支付，例如支付宝，微信，银联等。

5．支付与优惠活动管理
支付宝与微信，银联都均支持免密支付(无感支付)。本系统自带优惠券功能，支持支持多种套餐自定义与用户进行快捷手机上下单随时购买

（二）特色功能
1．异常数据实时推送，汇报，及时处理，提前预知与通知
2．即时通讯功能(IM聊天沟通)
性能，架构，优化等均参考微信聊天功能机制进行开发

3．行业好友与圈子
让该应用不止只能停车，还能交到志同道合的行业知音，让应用更有温度

4．商城与营销功能
此功能主要考虑到使用者有运营周边的兴趣和能力，在商城和广告营销上进行盈利



#### 安装教程

1.  安装JDK1.8+
2.  安装MySQL5.6+ 安装MongoDB  安装Redis  安装FastDFS 安装Zookeeper
3.  将打包好的代码上传到服务器上，直接运行jar包即可
4.  详细安装教程文档地址 https://www.showdoc.cc/902821218399318?page_id=4807644925521516

#### 使用说明(swagger2文档)

【用户基础数据相关】：http://47.100.11.151:8080/swagger-ui.html
【后台用户/角色/权限】：http://47.100.11.151:8091/swagger-ui.html

【短信】：http://47.100.11.151:8085/swagger-ui.html
【文件上传】：http://47.100.11.151:8088/swagger-ui.html

【停车场前端】：http://47.100.11.151:8089/swagger-ui.html
【停车场后台】：http://47.100.11.151:8092/swagger-ui.html

【支付前端】：http://47.100.11.151:8096/swagger-ui.html
【支付后台】：http://47.100.11.151:8097/swagger-ui.html

【广告管理相关】：http://47.100.11.151:8100/swagger-ui.html
【广告前端展示相关】：http://47.100.11.151:8099/swagger-ui.html

【充电桩管理相关】：http://47.100.11.151:8102/swagger-ui.html
【充电桩前端展示相关】：http://47.100.11.151:8103/swagger-ui.html

#### 国内领先水平  专业演示




#### 演示地址
http://test.admin.cfeng.wang/
test   123456  【建议不要随意修改数据，免得影响他人查看效果，非常感谢】

如果您发现有代码有什么不足之处请跟我留言，如果我留言不及时请加我个人微信Dove981011512



